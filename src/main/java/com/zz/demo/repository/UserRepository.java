package com.zz.demo.repository;

import org.springframework.data.repository.CrudRepository;
import com.zz.demo.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
