package com.zz.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.zz.demo.entity.Reservation;

public interface ReservationRepository extends CrudRepository<Reservation, Long> {

}
