package com.zz.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.demo.entity.User;
import com.zz.demo.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public Iterable<User> getUsers() {
		return userRepository.findAll();
	}
	
	public User getUser(Long id) {
		return userRepository.findById(id).get();
	}
	
	public User createUser(User user) {
		return userRepository.save(user);
	}
	
	public User updateUser(User user, Long id) {
		user.setId(id);
		return userRepository.save(user);
	}
	
	public boolean deleteUser(Long userId) {
		
		try {
			userRepository.delete(userRepository.findById(userId).get());
			return true;
		}catch (Exception e) {
			return false;
		}
		
	}

}
