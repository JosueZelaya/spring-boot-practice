package com.zz.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zz.demo.entity.Reservation;
import com.zz.demo.repository.ReservationRepository;

@Service
public class ReservationService {

	@Autowired
	ReservationRepository reservationRepository;

	public Iterable<Reservation> getReservations() {
		return reservationRepository.findAll();
	}

	public Reservation getReservation(Long id) {
		return reservationRepository.findById(id).get();
	}

	public Reservation createReservation(Reservation Reservation) {
		return reservationRepository.save(Reservation);
	}

	public Reservation updateReservation(Reservation Reservation, Long id) {
		Reservation.setId(id);
		return reservationRepository.save(Reservation);
	}

	public boolean deleteReservation(Long reservationId) {

		try {
			reservationRepository.delete(reservationRepository.findById(reservationId).get());
			return true;
		} catch (Exception e) {
			return false;
		}

	}

}
