package com.zz.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zz.demo.entity.Reservation;
import com.zz.demo.service.ReservationService;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

	@Autowired
	ReservationService reservationService;

	@GetMapping
	public Iterable<Reservation> getAllReservations() {
		return reservationService.getReservations();
	}

	@GetMapping("/{reservationId}")
	public Reservation getReservationById(@PathVariable Long reservationId) {
		return reservationService.getReservation(reservationId);
	}

	@PostMapping
	public Reservation createReservation(@RequestBody Reservation reservation) {
		return reservationService.createReservation(reservation);
	}

	@PutMapping("/{reservationId}")
	public Reservation updateReservation(@PathVariable Long reservationId, @RequestBody Reservation reservation) {
		return reservationService.updateReservation(reservation, reservationId);
	}

	@DeleteMapping("/{reservationId}")
	public boolean deleteReservation(@PathVariable Long reservationId) {
		return reservationService.deleteReservation(reservationId);
	}

}
